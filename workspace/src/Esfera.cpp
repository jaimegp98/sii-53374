//  CLASE CREADA POR: JAIME GONZÁLEZ PLAZA 53374    //


// Esfera.cpp: implementation of the Esfera class.


#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=1.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
        if (radio>0.15){
        radio-=0.01f;
        }
        
        else radio=0.15f;
        
        glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
	
}

void Esfera::Mueve(float t)
{
    centro.x=centro.x + velocidad.x*t;
    centro.y=centro.y + velocidad.y*t;
}
/////////////
